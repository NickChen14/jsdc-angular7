import { Component, Input } from '@angular/core';
import { R3_VIEW_CONTAINER_REF_FACTORY } from '@angular/core/src/ivy_switch/runtime/legacy';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  appname = '待辦事項';
  inputhint = '請輸入工作';
  item: string;
  checkall: boolean;
  data = [
    {
      'id': 1,
      'title': '準備 GDG DevFest Taipei 2018 演講',
      'isDone': false
    },
    {
      'id': 2,
      'title': '準備 JSDC 2018 演講',
      'isDone': false
    },
    {
      'id': 3,
      'title': '準備 Angular Taiwan 2018 演講',
      'isDone': true
    }
  ];

  markDone(id: number) {
    this.data = this.data.map( v => {
      if (v.id === id) {
        v = Object.assign({}, v);
        v.isDone = !v.isDone;
      }
      return v;
    });
  }

  removeItem(id: number) {
    this.data = this.data.filter( v => {
      return v.id !== id;
    });
  }

  markAll() {
    console.log(this.checkall);
    this.data.map( v => {
      v.isDone = this.checkall;
      return v;
    });
  }

  addItem() {
    this.data = [...this.data, {
        id: this.data.length + 1,
        title: this.item,
        isDone: false
    }];
    this.item = '';
    console.log(this.data);
  }
}
